//
//  ViewController.swift
//  calculator2
//
//  Created by Bhasker on 1/13/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    var str = ""   // for gathering numbers
    var firstNum = 0.0  // for storing first entered number and answer also
    var secNum = 0.0
    var isNumEntered = false
    var result = 0.0
    var op = "" // operation selected
    var isOperationPressedAgain = false
    var flag = 0 // to differentiate if operator prssed or equal to
    
    
    //  two labels used for output
    
    @IBOutlet weak var output: UILabel!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    // any number tapped
    @IBAction func number(sender: AnyObject) {
        
        var num = sender.currentTitle
        str = str + num!!
        output.text = output.text! + num!!
        isNumEntered = true

    }
    
    // any calc. operator tapped
    @IBAction func operation(sender: AnyObject) {
        
        if (isNumEntered) {
            
            if ( !isOperationPressedAgain ) {
            
            op = sender.currentTitle!!
        
            output.text = output.text! + op
        
            firstNum = NSString(string: str).doubleValue
            
            str = ""
            isNumEntered = false
            isOperationPressedAgain = true
            
            }
            else { //compute answer and store in firstNum
            
            flag = 1
            equals( isNumEntered )
            
            op = sender.currentTitle!!
            output.text = output.text! + op
            flag = 0
            
            }
        
        }
    }
    
    // equal to pressed
    @IBAction func equals(sender: AnyObject) {
        
        if ( isNumEntered ) {
            secNum = NSString(string: str).doubleValue
        
            switch op {
            
                case "+":
                    result = firstNum + secNum
            
                case "-":
                    result = firstNum - secNum
            
                case "*":
                    result = firstNum * secNum
        
                case "/":
                    if (secNum != 0 ) {
                        result = firstNum / secNum
                    } else {
                        result = 0
                    }
                    break
                
                default :
                    result = secNum
            
            }
            
            firstNum = result
            str = ""
            isNumEntered = false
            
            if (flag == 0) {
            
                resultLabel.text = output.text! + " = \(result)"
                output.text = ""
                isOperationPressedAgain = false
            }
        }
        
    }
    
    
    // clear all variables
    
    @IBAction func clear(sender: AnyObject) {
        
        output.text = ""
        resultLabel.text = ""
        str = ""
        firstNum = 0.0
        secNum = 0.0
        isNumEntered = false
        op = ""
        result = 0.0
        isOperationPressedAgain = false
        flag = 0
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

